﻿namespace CADto
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines the CA audit entry operation.
    /// </summary>
    [DataContract(Name = "CAAuditEntryOperation")]
    public enum CAAuditEntryOperation
    {
        /// <summary>
        /// None member
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// Create member
        /// </summary>
        [EnumMember]
        Create = 1,

        /// <summary>
        /// Read member
        /// </summary>
        [EnumMember]
        Read = 2,

        /// <summary>
        /// Update member
        /// </summary>
        [EnumMember]
        Update = 3,

        /// <summary>
        /// Delete member
        /// </summary>
        [EnumMember]
        Delete = 4
    }
}