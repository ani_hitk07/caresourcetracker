﻿
namespace CADto
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines the CA audit entry source.
    /// </summary>
    [DataContract(Name = "CAAuditEntrySource")]
    public enum CAAuditEntrySource
    {
        /// <summary>
        /// None member
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// CA application
        /// </summary>
        [EnumMember]
        CA = 1,

        /// <summary>
        /// CA online
        /// </summary>
        [EnumMember]
        CAOnline = 2
    }
}