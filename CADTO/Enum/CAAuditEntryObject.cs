﻿
namespace CADto
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines the CA audit entry object.
    /// </summary>
    [DataContract(Name = "CAAuditEntryObject")]
    public enum CAAuditEntryObject
    {
        /// <summary>
        /// None member
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// CA ExperienceRecording
        /// </summary>
        [EnumMember]
        ExperienceRecording = 1,

        /// <summary>
        /// Consumable member
        /// </summary>
        [EnumMember]
        Consumable = 2,

        /// <summary>
        /// Background member
        /// </summary>
        [EnumMember]
        Background = 3,

        /// <summary>
        /// ShoppingList member
        /// </summary>
        [EnumMember]
        ShoppingList = 4
    }
}