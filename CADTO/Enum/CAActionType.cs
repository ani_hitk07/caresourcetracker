﻿
namespace CADto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Defines the CA Batch Action Type
    /// </summary>
    public enum CAActionType
    {
        /// <summary>
        /// None member
        /// </summary>
        None = 0,

        /// <summary>
        /// CA Create
        /// </summary>
        Create = 1,

        /// <summary>
        /// Rename member
        /// </summary>
        Rename = 2,

        /// <summary>
        /// Delete member
        /// </summary>
        Delete = 3,

        /// <summary>
        /// Add member
        /// </summary>
        Add = 4,

        /// <summary>
        /// Remove member
        /// </summary>
        Remove = 5,

        /// <summary>
        /// Break member
        /// </summary>
        Break = 6,

        /// <summary>
        /// Activate Member
        /// </summary>
        Active = 7,

        /// <summary>
        /// Deactivate Member
        /// </summary>
        Inactive = 8,

        /// <summary>
        /// Edit Member
        /// </summary>
        Edit = 9,

         /// <summary>
        /// Undelete /unarchive member
        /// </summary>
        Undelete = 10
    }
}
