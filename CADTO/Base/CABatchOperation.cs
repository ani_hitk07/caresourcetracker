﻿
namespace CADto
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using CADto;

    /// <summary>
    /// CA Batch Operation entity
    /// </summary>
    public class CABatchOperation
    {
        /// <summary>
        /// Gets or sets the type of the action.
        /// </summary>
        /// <value>
        /// The type of the action.
        /// </value>
        public string ActionType { get; set; }

        /// <summary>
        /// Gets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        public CAActionType Action
        {
            get
            {
                CAActionType retVal = CAActionType.None;
                try
                {
                    int intActionType;
                    if (!string.IsNullOrEmpty(this.ActionType) && int.TryParse(this.ActionType, out intActionType) == true)
                    {
                        retVal = CAActionType.None;
                        this.Result = Resources.InappropriateAction;
                    }
                    else if (!string.IsNullOrEmpty(this.ActionType) && string.Compare(this.ActionType, Resources.NoActionString, true, CultureInfo.CurrentCulture) != 0)
                    {
                        retVal = (CAActionType)Enum.Parse(typeof(CAActionType), this.ActionType, true);
                    }
                    else if (!string.IsNullOrEmpty(this.ActionType) && string.Compare(this.ActionType, Resources.NoActionString, true, CultureInfo.CurrentCulture) == 0)
                    {
                        this.Result = Resources.NoActionUserMessage;
                    }
                }
                catch (ArgumentNullException)
                {
                    retVal = CAActionType.None;
                    this.Result = Resources.InappropriateAction;
                }
                catch (ArgumentException)
                {
                    retVal = CAActionType.None;
                    this.Result = Resources.InappropriateAction;
                }
                catch (OverflowException)
                {
                    retVal = CAActionType.None;
                    this.Result = Resources.InappropriateAction;
                }

                return retVal;
            }
        }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public string Result { get; set; }
    }
}
