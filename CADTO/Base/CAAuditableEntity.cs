﻿
namespace CADto
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// CA auditable entity
    /// </summary>
    [Serializable]
    [DataContract(Name = "CAAuditableEntity")]
    public class CAAuditableEntity : CABaseEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CAAuditableEntity"/> class.
        /// </summary>
        public CAAuditableEntity()
        {
            this.CreatedOn = DateTime.UtcNow;
            this.ModifiedOn = DateTime.UtcNow;
        }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember(Name = "CreatedBy")]
        public CAPrincipal CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        [DataMember(Name = "ModifiedBy")]
        public CAPrincipal ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>
        /// The created on.
        /// </value>
        [DataMember(Name = "CreatedOn")]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the modified on.
        /// </summary>
        /// <value>
        /// The modified on.
        /// </value>
        [DataMember(Name = "ModifiedOn")]
        public DateTime ModifiedOn { get; set; }
    }
}
