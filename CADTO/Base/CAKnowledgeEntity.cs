﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CAKnowledgeEntity.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   Defines the CA knowledge type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CADto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// CA knowledge entity
    /// </summary>
    [Serializable]
    [DataContract(Name = "Knowledge")]
    public class CAKnowledgeEntity : CAAuditableEntity
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is only resource.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is only resource; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "IsResource")]
        public bool IsResource { get; set; }

        /// <summary>
        /// Gets or sets the resource.
        /// </summary>
        /// <value>
        /// The resource.
        /// </value>
        [DataMember(Name = "Resource")]
        public CAResource Resource { get; set; }

        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        /// <value>
        /// The audience.
        /// </value>
        [DataMember(Name = "Audience")]
        public CAAudience Audience { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember(Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is dirty.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is dirty; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "IsDirty")]
        public bool IsDirty { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is added.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is added; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "IsAdded")]
        public bool IsAdded { get; set; }

        /// <summary>
        /// Gets or sets the current principal premission.
        /// </summary>
        /// <value>
        /// The current principal premission.
        /// </value>
        [DataMember(Name = "CurrentPrincipalPermissions")]
        public CAAuthorizationPermissions CurrentPrincipalPermissions { get; set; }
    }
}