﻿
namespace CADto
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// CA base object
    /// </summary>
    [Serializable]
    [DataContract(Name = "CABaseObject")]
    public class CABaseObject
    {
    }
}
