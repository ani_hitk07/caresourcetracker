﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CAResource.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   Defines the CA resource type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CADto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Web.Script.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// CA resource entity
    /// </summary>
    [Serializable]
    [DataContract(Name = "CAResource")]
    public class CAResource
    {
        /// <summary>
        /// Resource uri field
        /// </summary>
        [XmlIgnore]
        [ScriptIgnore]
        private Uri resourceUri;

        /// <summary>
        /// Gets the resource URL string.
        /// </summary>
        /// <value>
        /// The resource URL.
        /// </value>
        [DataMember(Name = "Resource")]
        [XmlIgnore]
        public string Resource { get; private set; }

        /// <summary>
        /// Gets or sets the resource URI.
        /// </summary>
        /// <value>
        /// The resource URI.
        /// </value>
        [XmlIgnore]
        [ScriptIgnore]
        public Uri ResourceUri
        {
            get
            {
                return this.resourceUri;
            }

            set
            {
                if (value != null)
                {
                    this.resourceUri = value;
                    this.Resource = this.resourceUri.ToString();
                }
            }
        }

        /// <summary>
        /// Gets or sets the status message.
        /// </summary>
        /// <value>
        /// The status message.
        /// </value>
        [DataMember(Name = "StatusMessage")]
        public string StatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>
        /// The status code.
        /// </value>
        [DataMember(Name = "StatusCode")]
        public HttpStatusCode StatusCode { get; set; }

        [DataMember(Name = "TicketId", IsRequired = false)]
        public string TicketId { get; set; }
    }
}
