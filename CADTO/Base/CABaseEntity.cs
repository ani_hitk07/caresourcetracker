﻿
namespace CADto
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Base entity class
    /// </summary>
    [Serializable]
    [DataContract(Name = "CABaseEntity")]
    public class CABaseEntity : CABaseObject
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember(Name = "Id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DataMember(Name = "Title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Siblings.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DataMember(Name = "isIncludeSiblings")]
        public bool isIncludeSiblings { get; set; }

        /// <summary>
        /// Gets or sets the ParentChild.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DataMember(Name = "isIncludeParentChild")]
        public bool isIncludeParentChild { get; set; }

    }
}