﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CAOptimizedAuditableEntity.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   Defines the optimized auditable entity class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CADto
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines the Security base class.
    /// </summary>
    [Serializable]
    [DataContract(Name = "CAOptimizedAuditableEntity")]
    public class CAOptimizedAuditableEntity : CABaseEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CAOptimizedAuditableEntity"/> class.
        /// </summary>
        public CAOptimizedAuditableEntity()
        {
            this.CreatedOn = DateTime.UtcNow;
            this.ModifiedOn = DateTime.UtcNow;
        }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public CAPrincipal CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        public CAPrincipal ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>
        /// The created on.
        /// </value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the modified on.
        /// </summary>
        /// <value>
        /// The modified on.
        /// </value>
        public DateTime ModifiedOn { get; set; }
    }
}