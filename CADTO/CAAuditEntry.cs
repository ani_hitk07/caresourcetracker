﻿namespace CADto
{
    using System;
    using System.Globalization;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// CA audit entry details
    /// </summary>
    [Serializable]
    [XmlType("CAAuditEntry")]
    public class CAAuditEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CAAuditEntry"/> class.
        /// </summary>
        public CAAuditEntry()
            : this(CAAuditEntrySource.None, CAAuditEntryOperation.None, CAAuditEntryObject.None, string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CAAuditEntry"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="operation">The operation.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="objectId">The object id.</param>
        public CAAuditEntry(CAAuditEntrySource source, CAAuditEntryOperation operation, CAAuditEntryObject objectType, string objectId)
        {
            this.Source = source;
            this.Operation = operation;
            this.ObjectType = objectType;
            this.ObjectId = objectId;
        }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        [XmlAttribute]
        public CAAuditEntrySource Source { get; set; }

        /// <summary>
        /// Gets or sets the operation.
        /// </summary>
        /// <value>
        /// The operation.
        /// </value>
        [XmlAttribute]
        public CAAuditEntryOperation Operation { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>
        /// The type of the object.
        /// </value>
        [XmlAttribute]
        public CAAuditEntryObject ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>
        /// The object id.
        /// </value>
        [XmlAttribute]
        public string ObjectId { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string template = "<CAAuditEntry Source=\"{0}\" Operation=\"{1}\" ObjectType=\"{2}\" ObjectId=\"{3}\" />";
            return string.Format(CultureInfo.InvariantCulture, template, Enum.GetName(typeof(CAAuditEntrySource), this.Source), Enum.GetName(typeof(CAAuditEntryOperation), this.Operation), Enum.GetName(typeof(CAAuditEntryObject), this.ObjectType), this.ObjectId);
        }
    }
}