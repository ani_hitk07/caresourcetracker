﻿using ElasticSearchHelper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.DirectoryServices;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CAResourceTracker
{
    public partial class Allocationreportgeneration : Form
    {
        SearchResult<CAExcelData> result;
        SearchResult<DOIExcelData> doiresult;
        List<string> presentinAD = new List<string>();
        List<string> notpresentinAD = new List<string>();

        public Allocationreportgeneration()
        {
            InitializeComponent();
        }
        public Allocationreportgeneration(SearchResult<CAExcelData> frmRes, SearchResult<DOIExcelData> frmDoiRes)
        {
            result = frmRes;
            doiresult = frmDoiRes;

            InitializeComponent();
        }

        private void Analysis_Load(object sender, EventArgs e)
        {
            var aIDs = result.Resultdatatable.AsEnumerable().Select(r => r.Field<string>("AssociateID"));
            var bIDs = doiresult.Resultdatatable.AsEnumerable().Select(r => r.Field<string>("CognizantID"));
            var diff = aIDs.Except(bIDs);
            DataTable tblDiff = (from r in result.Resultdatatable.AsEnumerable()
                                 join dId in diff on r.Field<string>("AssociateID") equals dId
                                 select r).CopyToDataTable();

            dgvUTIYDOIN.DataSource = tblDiff;

            diff = bIDs.Except(aIDs);

            tblDiff = (from r in doiresult.Resultdatatable.AsEnumerable()
                       join dId in diff on r.Field<string>("CognizantID") equals dId
                       select r).CopyToDataTable();

            dgvDOIYUTIN.DataSource = tblDiff;

            DataTable diffdt = new DataTable();
            diffdt = result.Resultdatatable.Clone();

            diffdt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList()
               .ForEach(cn =>
               {
                   if (cn.ToString().ToLowerInvariant().EndsWith("date"))
                   {
                       diffdt.Columns[cn].DataType = typeof(DateTime);
                       diffdt.Columns[cn].AllowDBNull = true;
                   }
               });

            diffdt = result.Resultdatatable.Copy();

            var mylinq = diffdt.AsEnumerable()
                            .GroupBy(r => new { AssociateID = r.Field<string>("AssociateID"), AssociateName = r.Field<string>("AssociateName") })
                            .Select(grp => new
                            {
                                AssociateID = grp.Key.AssociateID,
                                AssociateName = grp.Key.AssociateName,
                                AllocationEndDate = grp.Max(d => DateTime.Parse(d.Field<string>("AllocationEndDate")).Date.ToString("MM/dd/yyyy"))
                            });

            string expression = string.Empty;
            expression = string.Format("AllocationEndDate < #{0}#",
                         DateTime.Now.ToString("MM/dd/yyyy"));
            DataRow[] foundRows;
            foundRows = mylinq.AsEnumerable().ToDataTable().Select(expression);
            if (foundRows.Length > 0)
            {
                var dateresulttable = foundRows.CopyToDataTable();
                dgvUTIMDLSD.DataSource = dateresulttable;
            }

        }

        private void DownloadToExcel(DataGridView dg)
        {
            DataTable dt = new DataTable();

            foreach (DataGridViewColumn column in dg.Columns)
            {
                dt.Columns.Add(column.HeaderText, column.ValueType);
            }

            //Adding the Rows
            foreach (DataGridViewRow row in dg.Rows)
            {
                dt.Rows.Add();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    DateTime dateTime;
                    dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value == null ? string.Empty : (DateTime.TryParse(cell.Value.ToString(), out dateTime)) ? dateTime.Date.ToString("MM/dd/yyyy") : cell.Value.ToString();
                }
            }

            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "xlsx";
            saveFileDialog.Filter = "Excel files (*.xlsx)|*.xlsx |All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                //const string MyFileName = "filename.xlsx";
                //string execPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Details");
                    ws.Cells["A1"].LoadFromDataTable(dt, true);
                    ws.Cells.Style.Font.SetFromFont(new Font("Calibri", 10));
                    ws.Cells.AutoFitColumns();
                    //Format the header    
                    using (ExcelRange objRange = ws.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        objRange.Style.Fill.BackgroundColor.SetColor(Color.FloralWhite);
                    }
                    if (File.Exists(saveFileDialog.FileName))
                        File.Delete(saveFileDialog.FileName);

                    //Create excel file on physical disk    
                    FileStream objFileStrm = File.Create(saveFileDialog.FileName);
                    objFileStrm.Close();

                    //Write content to excel file    
                    File.WriteAllBytes(saveFileDialog.FileName, pck.GetAsByteArray());
                }

                MessageBox.Show("File Downloaded Sucessfully to - " + Path.GetDirectoryName(saveFileDialog.FileName));
            }
        }

        private void QueryAD(DataGridView dg)
        {
            DataTable dt = new DataTable();

            foreach (DataGridViewColumn column in dg.Columns)
            {
                dt.Columns.Add(column.HeaderText, column.ValueType);
            }

            //Adding the Rows
            foreach (DataGridViewRow row in dg.Rows)
            {
                dt.Rows.Add();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    DateTime dateTime;
                    dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value == null ? string.Empty : (DateTime.TryParse(cell.Value.ToString(), out dateTime)) ? dateTime.Date.ToString("MM/dd/yyyy") : cell.Value.ToString();
                }
            }

            foreach (DataRow dr in dt.Rows)
            {
                string name = dr["AssociateName"].ToString();

                LDAPQuery(name);
            }

            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "xlsx";
            saveFileDialog.Filter = "Excel files (*.xlsx)|*.xlsx |All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                //const string MyFileName = "filename.xlsx";
                //string execPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Details");
                    ws.Cells["A1"].LoadFromCollection(presentinAD, true);
                    ws.Cells.Style.Font.SetFromFont(new Font("Calibri", 10));
                    ws.Cells.AutoFitColumns();
                    //Format the header    
                    using (ExcelRange objRange = ws.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        objRange.Style.Fill.BackgroundColor.SetColor(Color.FloralWhite);
                    }
                    if (File.Exists(saveFileDialog.FileName))
                        File.Delete(saveFileDialog.FileName);

                    //Create excel file on physical disk    
                    FileStream objFileStrm = File.Create(saveFileDialog.FileName);
                    objFileStrm.Close();

                    //Write content to excel file    
                    File.WriteAllBytes(saveFileDialog.FileName, pck.GetAsByteArray());
                }

                MessageBox.Show("File Downloaded Sucessfully to - " + Path.GetDirectoryName(saveFileDialog.FileName));
            }

        }

        private void LDAPQuery(string name)
        {
            using (DirectoryEntry entry = new DirectoryEntry("LDAP://ucles.internal/DC=ucles,DC=internal", "A2CLdapR@ucles.internal", "R34d1ng15Fun!"))
            {
                entry.AuthenticationType = AuthenticationTypes.None;
                string userSearchFilter = string.Format(CultureInfo.InvariantCulture, "(&(objectCategory=user)(objectClass=user)(!(msExchResourceMetaData=ResourceType:Room))(|(SAMAccountName={0}*)(displayName={0}*)(givenName={0}*)(sn={0}*)(mail={0}*)))", name);
                using (DirectorySearcher dirSearch = new DirectorySearcher(entry, userSearchFilter))
                {
                    dirSearch.SearchScope = SearchScope.Subtree;

                    dirSearch.PropertiesToLoad.Add("sAMAccountName");
                    dirSearch.PropertiesToLoad.Add("mail");
                    dirSearch.PropertiesToLoad.Add("displayName");
                    dirSearch.PropertiesToLoad.Add("givenName");
                    dirSearch.PropertiesToLoad.Add("sn");

                    dirSearch.ReferralChasing = ReferralChasingOption.All;

                    using (SearchResultCollection cResults = dirSearch.FindAll())
                    {
                        if (cResults != null)
                        {
                            foreach (SearchResult theCurrentResult in cResults)
                            {
                                ResultPropertyCollection oProperties = null;
                                oProperties = theCurrentResult.Properties;
                                if (oProperties.Contains("sAMAccountName") && oProperties.Contains("mail") && oProperties.Contains("displayName") && oProperties.Contains("givenName") && oProperties.Contains("sn"))
                                    presentinAD.Add(name);
                                else
                                    notpresentinAD.Add(name);

                            }
                        }
                    }
                }
            }
        }
        private void Analysis_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            WindowState = FormWindowState.Minimized;
        }

        private void CAYDOIN_Click(object sender, EventArgs e)
        {
            DownloadToExcel(dgvUTIYDOIN);
        }

        private void DOIYCAN_Click(object sender, EventArgs e)
        {
            DownloadToExcel(dgvDOIYUTIN);
        }

        private void UTIMALSYSD_Click(object sender, EventArgs e)
        {
            DownloadToExcel(dgvUTIMDLSD);
        }

        private void ADDEF_Click(object sender, EventArgs e)
        {
            QueryAD(dgvUTIYDOIN);
        }
    }
}
