﻿using ElasticSearchHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CAResourceTracker
{
    public partial class Activeallocation : Form
    {
        SearchResult<CAExcelData> result = null;
        SearchResult<DOIExcelData> doiresult = null;

        List<string> tags = new List<string>();
        AutoCompleteStringCollection atcn = new AutoCompleteStringCollection();


        public Activeallocation()
        {
            InitializeComponent();
        }

        private void CA_Load(object sender, EventArgs e)
        {
            LoadMenuItems();

            var indexService = new ElasticIndexService(ElasticConfig.AllresourceindexName);
            result = indexService.SearchIndex(ElasticConfig.AllresourceindexName, tags, false, string.Empty);
            constructGridDetails(result, false);

            indexService = new ElasticIndexService(ElasticConfig.DOIresourceindexName);
            doiresult = indexService.DOISearchIndex(ElasticConfig.DOIresourceindexName, tags, false, string.Empty);
            constructDOIGridDetails(doiresult, false);

            textBox1.AutoCompleteCustomSource = atcn;

        }
        private void LoadMenuItems()
        {
            this.Menu = new MainMenu();
            MenuItem item = new MenuItem("CA");
            this.Menu.MenuItems.Add(item);
            item.Click += CAItem_Click;

            item = new MenuItem("Analysis");
            this.Menu.MenuItems.Add(item);
            item.Click += Anltem_Click;

        }


        private void CAItem_Click(object sender, EventArgs e)
        {
            Activeallocation ca = new Activeallocation();
            ca.Show();
        }

        private void Anltem_Click(object sender, EventArgs e)
        {
            Allocationreportgeneration an = new Allocationreportgeneration(result, doiresult);
            an.Show();
        }

        private void LinkedLabelClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tags.Clear();
            LinkLabel lnk = sender as LinkLabel;
            var text = lnk.Tag.ToString();
            tags.Add(text);
            var indexService = new ElasticIndexService(ElasticConfig.AllresourceindexName);
            var result = indexService.SearchIndex(ElasticConfig.AllresourceindexName, tags, true, string.Empty);
            if (tags.Any(str => str.Contains("<< Reset Filter")))
                constructGridDetails(result, false);
            else
                constructGridDetails(result, true);

        }


        private void constructGridDetails(SearchResult<CAExcelData> result, bool isfilter)
        {
            if (isfilter)
                result.AggregationsByTags.Add("<< Reset Filter", 0);
            tblPnlDataEntry.Controls.Clear();
            tblPnlDataEntry.RowCount = result.AggregationsByTags.Count;
            int currentRow = 0;
            foreach (KeyValuePair<string, long> link in result.AggregationsByTags)
            {
                var linkLabel1 = new LinkLabel();
                if (isfilter)
                    linkLabel1.Text = link.Key;
                else
                    linkLabel1.Text = link.Key + " (" + link.Value + ")";
                linkLabel1.Tag = link.Key;
                linkLabel1.LinkBehavior = LinkBehavior.HoverUnderline;
                linkLabel1.ForeColor = Color.Blue;
                linkLabel1.AutoSize = true;
                linkLabel1.Font = new Font("Georgia", 10);
                linkLabel1.Links.Add(1, 1, link.Key);
                linkLabel1.Width = 700;
                linkLabel1.LinkArea = new LinkArea(0, link.Key.Length);
                linkLabel1.LinkClicked += new LinkLabelLinkClickedEventHandler(LinkedLabelClicked);
                linkLabel1.ActiveLinkColor = Color.Orange;
                linkLabel1.VisitedLinkColor = Color.Green;
                linkLabel1.LinkColor = Color.RoyalBlue;
                linkLabel1.DisabledLinkColor = Color.Gray;

                tblPnlDataEntry.Controls.Add(linkLabel1, 0, currentRow++);
            }
            dgvCA.DataSource = result.Resultdatatable;
        }

        private void constructDOIGridDetails(SearchResult<DOIExcelData> result, bool isfilter)
        {
            dgvDOI.DataSource = result.Resultdatatable;
        }

        private void CA_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            WindowState = FormWindowState.Minimized;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Length >= 3)
            {
                var indexService = new ElasticIndexService(ElasticConfig.AllresourceindexName);
                var suggestions = indexService.Suggest(textBox1.Text);
                suggestions.ToList().ForEach(r => atcn.Add(r));
            }
        }
    }
}
