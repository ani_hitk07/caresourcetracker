﻿namespace CAResourceTracker
{
    partial class Allocationreportgeneration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvUTIYDOIN = new System.Windows.Forms.DataGridView();
            this.dgvDOIYUTIN = new System.Windows.Forms.DataGridView();
            this.dgvUTIMDLSD = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.UTIMALSYSD = new System.Windows.Forms.Button();
            this.CAYDOIN = new System.Windows.Forms.Button();
            this.DOIYCAN = new System.Windows.Forms.Button();
            this.ADDEF = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUTIYDOIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDOIYUTIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUTIMDLSD)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvUTIYDOIN
            // 
            this.dgvUTIYDOIN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUTIYDOIN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUTIYDOIN.Location = new System.Drawing.Point(3, 3);
            this.dgvUTIYDOIN.Name = "dgvUTIYDOIN";
            this.dgvUTIYDOIN.Size = new System.Drawing.Size(994, 202);
            this.dgvUTIYDOIN.TabIndex = 7;
            // 
            // dgvDOIYUTIN
            // 
            this.dgvDOIYUTIN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDOIYUTIN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDOIYUTIN.Location = new System.Drawing.Point(3, 211);
            this.dgvDOIYUTIN.Name = "dgvDOIYUTIN";
            this.dgvDOIYUTIN.Size = new System.Drawing.Size(994, 202);
            this.dgvDOIYUTIN.TabIndex = 8;
            // 
            // dgvUTIMDLSD
            // 
            this.dgvUTIMDLSD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUTIMDLSD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUTIMDLSD.Location = new System.Drawing.Point(3, 419);
            this.dgvUTIMDLSD.Name = "dgvUTIMDLSD";
            this.dgvUTIMDLSD.Size = new System.Drawing.Size(994, 202);
            this.dgvUTIMDLSD.TabIndex = 9;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dgvUTIYDOIN, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvUTIMDLSD, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dgvDOIYUTIN, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(30, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1000, 624);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel2.Controls.Add(this.CAYDOIN, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ADDEF, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.UTIMALSYSD, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.DOIYCAN, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1036, 15);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 621);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // UTIMALSYSD
            // 
            this.UTIMALSYSD.Location = new System.Drawing.Point(3, 383);
            this.UTIMALSYSD.Name = "UTIMALSYSD";
            this.UTIMALSYSD.Size = new System.Drawing.Size(108, 39);
            this.UTIMALSYSD.TabIndex = 2;
            this.UTIMALSYSD.Text = "Download to Excel";
            this.UTIMALSYSD.UseVisualStyleBackColor = true;
            this.UTIMALSYSD.Click += new System.EventHandler(this.UTIMALSYSD_Click);
            // 
            // CAYDOIN
            // 
            this.CAYDOIN.Location = new System.Drawing.Point(3, 3);
            this.CAYDOIN.Name = "CAYDOIN";
            this.CAYDOIN.Size = new System.Drawing.Size(108, 38);
            this.CAYDOIN.TabIndex = 0;
            this.CAYDOIN.Text = "Download to Excel";
            this.CAYDOIN.UseVisualStyleBackColor = true;
            this.CAYDOIN.Click += new System.EventHandler(this.CAYDOIN_Click);
            // 
            // DOIYCAN
            // 
            this.DOIYCAN.Location = new System.Drawing.Point(3, 193);
            this.DOIYCAN.Name = "DOIYCAN";
            this.DOIYCAN.Size = new System.Drawing.Size(108, 38);
            this.DOIYCAN.TabIndex = 3;
            this.DOIYCAN.Text = "Download to Excel";
            this.DOIYCAN.UseVisualStyleBackColor = true;
            this.DOIYCAN.Click += new System.EventHandler(this.DOIYCAN_Click);
            // 
            // ADDEF
            // 
            this.ADDEF.Location = new System.Drawing.Point(127, 3);
            this.ADDEF.Name = "ADDEF";
            this.ADDEF.Size = new System.Drawing.Size(70, 38);
            this.ADDEF.TabIndex = 4;
            this.ADDEF.Text = "AD Defaulters";
            this.ADDEF.UseVisualStyleBackColor = true;
            this.ADDEF.Click += new System.EventHandler(this.ADDEF_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 52);
            this.label1.TabIndex = 5;
            this.label1.Text = "This gives you the resource list present in CA dump but absent from DOI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 320);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 52);
            this.label2.TabIndex = 6;
            this.label2.Text = "This gives you the resource list present in DOI but absent from CA dump\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 510);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 52);
            this.label3.TabIndex = 7;
            this.label3.Text = "This gives you the reource list whose max allocation date is less than sys date";
            // 
            // Allocationreportgeneration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 648);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Allocationreportgeneration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Analysis";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Analysis_FormClosing);
            this.Load += new System.EventHandler(this.Analysis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUTIYDOIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDOIYUTIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUTIMDLSD)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvUTIYDOIN;
        private System.Windows.Forms.DataGridView dgvDOIYUTIN;
        private System.Windows.Forms.DataGridView dgvUTIMDLSD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button UTIMALSYSD;
        private System.Windows.Forms.Button CAYDOIN;
        private System.Windows.Forms.Button DOIYCAN;
        private System.Windows.Forms.Button ADDEF;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
    }
}