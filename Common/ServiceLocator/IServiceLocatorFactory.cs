// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceLocatorFactory.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   Interface for classes that will create and configure service locators, in such a way that
//   they can be used by the <see cref="GeneralServiceLocator" />.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Ericsson.CA.Common
{
    using System.Collections.Generic;

    using Microsoft.Practices.ServiceLocation;

    /// <summary>
    /// Interface for classes that will create and configure service locators, in such a way that
    /// they can be used by the <see cref="GeneralServiceLocator"/>.
    /// </summary>
    public interface IServiceLocatorFactory
    {
        /// <summary>
        /// Create the <see cref="IServiceLocator"/>
        /// </summary>
        /// <returns>The created service locator</returns>
        IServiceLocator Create();

        /// <summary>
        /// Loads the type mappings into the service locator. 
        /// </summary>
        /// <param name="serviceLocator">The service locator to load type mappings into.</param>
        /// <param name="typeMappings">The type mappings to load</param>
        void LoadTypeMappings(IServiceLocator serviceLocator, IEnumerable<TypeMapping> typeMappings);
    }
}