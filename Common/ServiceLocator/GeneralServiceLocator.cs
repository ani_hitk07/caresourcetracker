﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GeneralServiceLocator.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   Defines the ServiceLocator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Ericsson.CA.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Microsoft.Practices.EnterpriseLibrary.PolicyInjection.CallHandlers;
    using Microsoft.Practices.ServiceLocation;
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;

    /// <summary>
    /// Class that manages a single instance of of a service locator.
    /// </summary>
    /// <remarks>In future, if required make the service location configurable</remarks>
    public class GeneralServiceLocator
    {
        /// <summary>
        /// Assume audit is enabled by default
        /// </summary>
        private static bool isAuditEnabled = true;

        /// <summary>
        /// Assume tracing is disabled by default
        /// </summary>
        private static bool isInstrumentationEnabled = false;

        /// <summary>
        /// Assuming performance trace is disabled by default
        /// </summary>
        private static bool isPerformanceTraceEnabled = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GeneralServiceLocator"/> class.
        /// </summary>
        /// <param name="defaultMappings">The default mappings.</param>
        public GeneralServiceLocator(IEnumerable<TypeMapping> defaultMappings)
        {
            try
            {
                CASharePointConfigValueRepository ConfigValue = new CASharePointConfigValueRepository();

                // Auditing
                bool retVal = true;
                string configValue = ConfigValue.ReadConfigValueValue(ConfigurationConstant.IsAuditingEnabled);

                if (string.IsNullOrEmpty(configValue) == false)
                {
                    if (bool.TryParse(configValue, out retVal) == false)
                    {
                        retVal = true;
                    }
                }

                isAuditEnabled = retVal;

                // Instrumentation
                retVal = false;
                //retVal = true;

                configValue = ConfigValue.ReadConfigValueValue(ConfigurationConstant.IsInstrumentationEnabled);

                if (string.IsNullOrEmpty(configValue) == false)
                {
                    if (bool.TryParse(configValue, out retVal) == false)
                    {
                        retVal = false;
                    }
                }

                isInstrumentationEnabled = retVal;

                // Performance trace
                retVal = false;
                //retVal = true;
                configValue = ConfigValue.ReadConfigValueValue(ConfigurationConstant.IsPerformanceTraceEnabled);
                if (string.IsNullOrEmpty(configValue) == false)
                {
                    if (bool.TryParse(configValue, out retVal) == false)
                    {
                        retVal = false;
                    }
                }

                isPerformanceTraceEnabled = retVal;
            }
            catch (Exception ex)
            {
                CommonHelper.Logger.LogError(ex, TraceLogEventId.EventInCommon, false);
            }
            finally
            {
                this.CurrentLocator = GetCurrentServiceLocator(defaultMappings);
            }            
        }

        /// <summary>
        /// Gets the current locator.
        /// </summary>
        public IServiceLocator CurrentLocator { get; private set; }

        /// <summary>
        /// Make sure that the <see cref="ServiceLocator.Current"/> from the common service locator library doesn't work.
        /// It should fail with a <see cref="NotSupportedException"/>, because people should use <see cref="CurrentLocator"/>.
        /// </summary>
        private static void EnsureCommonServiceLocatorCurrentFails()
        {
            ServiceLocator.SetLocatorProvider(ThrowNotSupportedException);
        }

        /// <summary>
        /// Throws the not supported exception.
        /// </summary>
        /// <returns>Service locator instance</returns>
        private static IServiceLocator ThrowNotSupportedException()
        {
            throw new NotSupportedException(Resources.ServiceLocatorNotSupported);
        }

        /// <summary>
        /// Gets the current service locator.
        /// </summary>
        /// <param name="defaultMappings">The default mappings.</param>
        /// <returns>
        /// Service locator instance
        /// </returns>
        private static IServiceLocator GetCurrentServiceLocator(IEnumerable<TypeMapping> defaultMappings)
        {
            EnsureCommonServiceLocatorCurrentFails();
            return CreateServiceLocatorInstance(defaultMappings);
        }

        /// <summary>
        /// Create a new instance of the service locator and possibly fill it with
        /// </summary>
        /// <param name="defaultMappings">The default mappings.</param>
        /// <returns>
        /// Service locator instance
        /// </returns>
        private static IServiceLocator CreateServiceLocatorInstance(IEnumerable<TypeMapping> defaultMappings)
        {
            UnityServiceLocator locator = new UnityServiceLocator(ConfigureUnityContainer(defaultMappings));
            ServiceLocator.SetLocatorProvider(() => locator);

            return ServiceLocator.Current;
        }

        /// <summary>
        /// Configures the unity container.
        /// </summary>
        /// <param name="defaultMappings">The default mappings.</param>
        /// <returns>
        /// Unity container with mapped types configured
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Per the P&P Unity sample code.")]
        private static IUnityContainer ConfigureUnityContainer(IEnumerable<TypeMapping> defaultMappings)
        {
            UnityContainer container = new UnityContainer();
            bool containsInterceptedTypes = false;

            if (defaultMappings != null)
            {
                foreach (var mapping in defaultMappings)
                {
                    Type fromType = Type.GetType(mapping.FromType);
                    Type toType = Type.GetType(mapping.ToType);

                    LifetimeManager mgr = mapping.InstantiationType == InstantiationType.AsSingleton ? ((LifetimeManager)new ContainerControlledLifetimeManager()) : ((LifetimeManager)new TransientLifetimeManager());

                    //// Use the default constructor
                    container.RegisterType(fromType, toType, mgr, new InjectionConstructor(), new InterceptionBehavior<PolicyInjectionBehavior>());

                    if ((string.IsNullOrEmpty(mapping.InterceptedType) == false) && (isAuditEnabled == true))
                    {
                        Type interceptedType = Type.GetType(mapping.InterceptedType);

                        if (interceptedType != null)
                        {
                            // At least one intercepted type is present
                            if (containsInterceptedTypes == false)
                            {
                                container.AddNewExtension<Interception>();
                                containsInterceptedTypes = true;
                            }

                            if (interceptedType.IsInterface == true)
                            {
                                container.Configure<Interception>().SetInterceptorFor(interceptedType, new InterfaceInterceptor());
                            }
                            else
                            {
                                container.Configure<Interception>().SetInterceptorFor(interceptedType, new VirtualMethodInterceptor());
                            }
                        }
                    }
                }

                ConfigurePerformanceTracingInstrumentation(container, containsInterceptedTypes, defaultMappings);
            }

            return container;
        }

        /// <summary>
        /// Configures the performance tracing and instrumentation.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="containsInterceptedTypes">if set to <c>true</c> [contains intercepted types].</param>
        /// <param name="defaultMappings">The default mappings.</param>
        private static void ConfigurePerformanceTracingInstrumentation(UnityContainer container, bool containsInterceptedTypes, IEnumerable<TypeMapping> defaultMappings)
        {
            if ((isInstrumentationEnabled == true) || (isPerformanceTraceEnabled == true))
            {
                if (containsInterceptedTypes == false)
                {
                    container.AddNewExtension<Interception>();
                }

                Dictionary<string, Type> typeMap = GetInterfacesForPerformanceTracingInstrumentation();

                foreach (var mapping in defaultMappings)
                {
                    Type fromType = Type.GetType(mapping.FromType);
                    Type toType = Type.GetType(mapping.ToType);

                    if (typeMap.ContainsKey(fromType.FullName) == true)
                    {
                        if ((string.IsNullOrEmpty(mapping.InterceptedType) == true) || (isAuditEnabled == false))
                        {
                            container.Configure<Interception>().SetInterceptorFor(toType, new VirtualMethodInterceptor());
                        }

                        AddTypePolicyForPerformance(container, toType, fromType);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the type policy for performance.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="implementedType">Type of the implemented.</param>
        /// <param name="interfaceType">Type of the interface.</param>
        private static void AddTypePolicyForPerformance(UnityContainer container, Type implementedType, Type interfaceType)
        {
            MemberName[] methodNames = GetInterfaceMethods(interfaceType);

            if ((methodNames != null) && (methodNames.Length > 0))
            {
                foreach (var methodName in methodNames)
                {
                    if (isInstrumentationEnabled == true)
                    {
                        container.Configure<Interception>()
                                .AddPolicy(methodName.FullName + "InstrumentationPolicy")
                                .AddMatchingRule<TypeMatchingRule>(new InjectionConstructor(implementedType.FullName, true))
                                .AddMatchingRule<MemberNameMatchingRule>(new InjectionConstructor(methodName.Name, true))
                                .AddCallHandler(new PerformanceCounterCallHandler("Ericsson.CA", methodName.FullName));
                    }

                    if (isPerformanceTraceEnabled == true)
                    {
                        container.Configure<Interception>()
                                .AddPolicy(methodName.FullName + "PerformanceTracePolicy")
                                .AddMatchingRule<TypeMatchingRule>(new InjectionConstructor(implementedType.FullName, true))
                                .AddMatchingRule<MemberNameMatchingRule>(new InjectionConstructor(methodName.Name, true))
                                .AddCallHandler(new CAPerformanceTraceCallHandler(methodName.Name, methodName.FullName));
                    }
                }
            }
        }

        /// <summary>
        /// Gets the interfaces for performance tracing and instrumentation.
        /// </summary>
        /// <returns>
        /// Array of interfaces to be traced
        /// </returns>
        private static Dictionary<string, Type> GetInterfacesForPerformanceTracingInstrumentation()
        {
            List<Type> retVal = new List<Type>();

            {
                // Common
                retVal.Add(typeof(ICAAuditRepository));
                retVal.Add(typeof(ICACache));
                retVal.Add(typeof(ICAConfigurationRepository));
                retVal.Add(typeof(ICASharePointConfigValueRepository));
                retVal.Add(typeof(ICAWebConfigurationRepository));

                // Data
                retVal.Add(typeof(ICAAuthenticationRepository));
                retVal.Add(typeof(ICAAuthorizationRepository));
                retVal.Add(typeof(ICABackgroundRepository));
                retVal.Add(typeof(ICAConsumableRepository));
                retVal.Add(typeof(ICAExperienceRecordingRepository));
                retVal.Add(typeof(ICAIncidentRepository));
                retVal.Add(typeof(ICAIntegrationProvider));
                retVal.Add(typeof(ICALoggingRepository));
                retVal.Add(typeof(ICAMasterDataRepository));
                retVal.Add(typeof(ICAOtherTagRepository));
                retVal.Add(typeof(ICAProductRepository));
                retVal.Add(typeof(ICASearchIndexRepository));
                retVal.Add(typeof(ICAShoppingListRepository));
                retVal.Add(typeof(ICASocialBookmarkRepository));
                retVal.Add(typeof(ICAStatementRepository));
                retVal.Add(typeof(ICASynonymsRepository));
                retVal.Add(typeof(ICALeaderboardRepository));
                retVal.Add(typeof(ICAHowTosRepository));
                // Service
                retVal.Add(typeof(ICAAuthorization));
                retVal.Add(typeof(ICAData));
                retVal.Add(typeof(ICAIntegration));
                retVal.Add(typeof(ICASearch));
            }

            Dictionary<string, Type> typesMap = new Dictionary<string, Type>();

            foreach (var retTyp in retVal)
            {
                typesMap.Add(retTyp.FullName, retTyp);
            }

            return typesMap;
        }

        /// <summary>
        /// Gets the interface method names.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <returns>Array of interface names</returns>
        private static MemberName[] GetInterfaceMethods(Type interfaceType)
        {
            List<MemberName> retVal = null;
            string qualifier = interfaceType.FullName + "_";

            MethodInfo[] methodInfos = interfaceType.GetMethods(BindingFlags.Public | BindingFlags.Instance);

            if (methodInfos != null)
            {
                retVal = new List<MemberName>();

                foreach (var method in methodInfos)
                {
                    string fullName = qualifier + method.Name;
                    fullName = (fullName.Length > 127) ? fullName.Substring(0, 126) : fullName;
                    retVal.Add(new MemberName(method.Name, fullName));
                }
            }

            return retVal.ToArray();
        }
    }
}