// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstantiationType.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   Determines how to instantiate objects from the service locator
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Ericsson.CA.Common
{
    using Microsoft.Practices.ServiceLocation;

    /// <summary>
    /// Determines how to instantiate objects from the service locator
    /// </summary>
    public enum InstantiationType
    {
        /// <summary>
        /// Create a new instance for each call to <see cref="IServiceLocator.GetInstance(System.Type)"/>. 
        /// </summary>
        NewInstanceForEachRequest,

        /// <summary>
        /// Create a singleton instance. Each call to <see cref="IServiceLocator.GetInstance(System.Type)"/> will return the same instance.
        /// </summary>
        AsSingleton
    }
}
