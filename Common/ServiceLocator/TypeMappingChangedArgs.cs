// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeMappingChangedArgs.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   Event args to use to notify a tpye mapping has changed for a service locator
//   (added programmatically)
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Ericsson.CA.Common
{
    using System;

    /// <summary>
    /// Event args to use to notify a tpye mapping has changed for a service locator
    /// (added programmatically)
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SharePoint Guidance 2010 code")]
    public class TypeMappingChangedArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the mapping.
        /// </summary>
        /// <value>
        /// The mapping.
        /// </value>
        public TypeMapping Mapping { get; set; }
    }
}
