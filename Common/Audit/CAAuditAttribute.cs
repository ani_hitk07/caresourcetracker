﻿
namespace CACommon
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using CADto;
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;

    /// <summary>
    /// CA audit details attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class CAAuditAttribute : HandlerAttribute
    {
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        [Required]
        public CAAuditEntrySource Source { get; set; }

        /// <summary>
        /// Gets or sets the operation.
        /// </summary>
        /// <value>
        /// The operation.
        /// </value>
        [Required]
        public CAAuditEntryOperation Operation { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>
        /// The type of the object.
        /// </value>
        [Required]
        public CAAuditEntryObject ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>
        /// The object id.
        /// </value>
        [Required]
        public string ObjectIdParameterName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [object id in return value].
        /// </summary>
        /// <value>
        /// <c>true</c> if [object id in return value]; otherwise, <c>false</c>.
        /// </value>
        [Required]
        public bool IsObjectIdInReturnValue { get; set; }

        /// <summary>
        /// Creates the handler.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <returns>The call handler object</returns>
        public override ICallHandler CreateHandler(IUnityContainer container)
        {
           // ICallHandler test=null;
            CAAuditEntry auditEntryObject = new CAAuditEntry(this.Source, this.Operation, this.ObjectType, string.Empty);

           return new CAAuditCallHandler(auditEntryObject, this.ObjectIdParameterName, this.IsObjectIdInReturnValue, Order);
           // return test;
        }
    }
}