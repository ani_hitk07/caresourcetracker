﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CAAuditRepository.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   CA audit repository
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Ericsson.CA.Common
{
    using System.Globalization;
    using Ericsson.CA.Common.Properties;
    using Ericsson.CA.Dto;
    using System;
    //

    /// <summary>
    /// CA audit repository
    /// </summary>
    public class CAAuditRepository : ICAAuditRepository
    {
        /// <summary>
        /// Audit event source name
        /// </summary>
        private const string AuditEventSourceName = "CACustomAuditEvent";

        /// <summary>
        /// Creates the audit entry.
        /// </summary>
        /// <param name="auditEntry">The audit entry.</param>
        public virtual void CreateAuditEntry(CAAuditEntry auditEntry)
        {
            try
            {
                if (auditEntry != null)
                {
                    //Flink change - Nedd to edit
                    //if ((SPContext.Current != null) && (SPContext.Current.Web != null))
                    //{
                    //    SPContext.Current.Web.Audit.WriteAuditEvent(SPAuditEventType.Custom, AuditEventSourceName, auditEntry.ToString());

                        
                    //}
                    //else
                    //{
                        string message = string.Format(CultureInfo.InvariantCulture, Resources.UnableToAudit, auditEntry.ToString());
                        CommonHelper.Logger.LogError(message, TraceLogEventId.EventInData, false);
                    //}
                }
                else
                {
                    string message = string.Format(CultureInfo.InvariantCulture, Resources.EmptyOrNullAuditEntry);
                    CommonHelper.Logger.LogError(message, TraceLogEventId.EventInData, false);
                }
            }
            catch(Exception ex)
            {
                CommonHelper.Logger.LogError(ex, TraceLogEventId.EventInData, false);
            }
        }
    }
}