﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CAAuditCallHandler.cs" company="Microsoft Corporation and Ericsson">
//   Copyright (c) Microsoft Corporation and Ericsson
// </copyright>
// <summary>
//   CA audit call handler
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Ericsson.CA.Common
{
    using System;
    using System.Globalization;
    using Ericsson.CA.Common.Properties;
    using Ericsson.CA.Dto;
    using Microsoft.Practices.Unity.InterceptionExtension;

    /// <summary>
    /// CA audit call handler
    /// </summary>
    public class CAAuditCallHandler : ICallHandler
    {
        /// <summary>
        /// Assume audit is enabled by default
        /// </summary>
        private static bool isAuditEnabled = IsAuditEnabled;

        /// <summary>
        /// Initializes a new instance of the <see cref="CAAuditCallHandler"/> class.
        /// </summary>
        /// <param name="auditEntry">The audit entry.</param>
        /// <param name="objectIdParameter">The object id parameter.</param>
        /// <param name="isObjectIdInReturnValue">if set to <c>true</c> [is object id in return value].</param>
        /// <param name="order">The order.</param>
        public CAAuditCallHandler(CAAuditEntry auditEntry, string objectIdParameter, bool isObjectIdInReturnValue, int order)
        {
            this.AuditEntry = auditEntry;
            this.ObjectIdParameter = objectIdParameter;
            this.IsObjectIdInReturnValue = isObjectIdInReturnValue;
            this.Order = order;
        }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is audit enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is audit enabled; otherwise, <c>false</c>.
        /// </value>
        private static bool IsAuditEnabled
        {
            get
            {
                bool retVal = true;

                string configValue = CommonHelper.SharePointConfigValueConfiguration.ReadConfigValueValue(ConfigurationConstant.IsAuditingEnabled);
                if (string.IsNullOrEmpty(configValue) == false)
                {
                    if (bool.TryParse(configValue, out retVal) == false)
                    {
                        CommonHelper.Logger.LogWarning(Resources.IsAuditEnabledMissing, TraceLogEventId.EventInCommon);
                        retVal = true;
                    }
                }

                return retVal;
            }
        }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        private CAAuditEntry AuditEntry { get; set; }

        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>
        /// The object id.
        /// </value>
        private string ObjectIdParameter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [object id in return value].
        /// </summary>
        /// <value>
        /// <c>true</c> if [object id in return value]; otherwise, <c>false</c>.
        /// </value>
        private bool IsObjectIdInReturnValue { get; set; }

        /// <summary>
        /// Invokes the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="getNext">The next method call.</param>
        /// <returns>Method call return object</returns>
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            IMethodReturn msg = null;

            try
            {
                if (getNext != null)
                {
                    // Call the next method in chain
                    msg = getNext()(input, getNext);
                }
            }
            finally
            {
                if (isAuditEnabled == true)
                {
                    this.AuditEntry.ObjectId = GetObjectId(input, msg, this.ObjectIdParameter, this.IsObjectIdInReturnValue);

                    ICAAuditRepository auditRepo = CACommonServiceLocator.CurrentServiceLocator.GetInstance<ICAAuditRepository>();

                    // Get the audit entry object id and set it in auditEntry object.
                    auditRepo.CreateAuditEntry(this.AuditEntry);
                }
            }

            return msg;
        }

        /// <summary>
        /// Gets the object id.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="isIdInReturnValue">if set to <c>true</c> [is id in return value].</param>
        /// <returns>
        /// Identifier of audited objects
        /// </returns>
        private static string GetObjectId(IMethodInvocation input, IMethodReturn msg, string parameterName, bool isIdInReturnValue)
        {
            string retVal = string.Empty;

            if (isIdInReturnValue == true)
            {
                retVal = GetIdFromReturnValue(msg, parameterName);
            }
            else
            {
                retVal = GetIdFromParameterValue(input, parameterName);
            }

            return retVal;
        }

        /// <summary>
        /// Gets the id from return value.
        /// </summary>
        /// <param name="msg">The abstracted return object from delegate call.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <returns>Identifier string</returns>
        private static string GetIdFromReturnValue(IMethodReturn msg, string parameterName)
        {
            string retVal = string.Empty;

            if ((msg != null) && (msg.ReturnValue != null))
            {
                try
                {
                    Type returnType = msg.ReturnValue.GetType();

                    if (returnType.Equals(typeof(string)) == true)
                    {
                        retVal = msg.ReturnValue.ToString();
                    }
                    else
                    {
                        retVal = ReflectionHelper.GetPropertyInfo(returnType, parameterName).GetValue(msg.ReturnValue, null).ToString();
                    }
                }
                catch (Exception ex)
                {
                    CommonHelper.Logger.LogError(ex, TraceLogEventId.EventInCommon, false);
                }
            }

            return retVal;
        }

        /// <summary>
        /// Gets the parameter value.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <returns>
        /// Id value from specified parameter
        /// </returns>
        private static string GetIdFromParameterValue(IMethodInvocation input, string parameterName)
        {
            string retVal = string.Empty;

            if ((input != null) && (string.IsNullOrEmpty(parameterName) == false))
            {
                try
                {
                    retVal = input.Inputs[parameterName].ToString();
                }
                catch (ArgumentException ex)
                {
                    string message = string.Format(CultureInfo.InvariantCulture, Resources.InvalidAuditRepositoryIdParameter, input.Target.GetType(), input.MethodBase.Name);
                    CommonHelper.Logger.LogError(ex, message, TraceLogEventId.EventInCommon, false);
                }
            }
            else
            {
                string message = string.Format(CultureInfo.InvariantCulture, Resources.InvalidAuditRepositoryIdParameter, input.Target.GetType(), input.MethodBase.Name);
                CommonHelper.Logger.LogError(message, TraceLogEventId.EventInCommon, false);
            }

            return retVal;
        }
    }
}