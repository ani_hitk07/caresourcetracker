﻿using Elasticsearch.Net;
using Nest;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.String;

namespace ElasticSearchHelper
{
    public class ElasticIndexService
    {
        private readonly IElasticClient client;

        public ElasticIndexService(string indexname)
        {
            client = ElasticConfig.GetClient(indexname);
        }

        public SearchResult<CAExcelData> SearchIndex(string indexname, List<string> tags, bool isfilter, string query)
        {
            SearchResult<CAExcelData> result = null;
            try
            {

                if (indexname == ElasticConfig.AllresourceindexName)
                {
                    if (!isfilter)
                    {
                        var scanResults = client.Search<CAExcelData>(s => s
                                               .Query(q => q
                                               .FunctionScore(fs => fs
                                               .Query(fq => fq.MatchAll())
                                               .Functions(f => f.RandomScore(Guid.NewGuid().ToString()))))
                                               .Aggregations(a => a
                                                            .Terms("by_projects", t => t
                                                                .Field(f => f.ProjectName)
                                                                .Size(10000)
                                                                ))
                                                .Size(10000));


                        result = new SearchResult<CAExcelData>
                        {
                            Total = (int)scanResults.Total,
                            Page = 1,
                            Results = scanResults.Documents,
                            ElapsedMilliseconds = scanResults.Took,
                            AggregationsByTags = scanResults.Aggs.Terms("by_projects").Buckets.ToDictionary(x => x.Key, y => y.DocCount.GetValueOrDefault(0)),
                            Resultdatatable = scanResults.Documents.ToDataTable()
                        };
                    }
                    else
                    {
                        var filters = tags.Select(c => new Func<QueryContainerDescriptor<CAExcelData>, QueryContainer>(x => x.Term(f => f.ProjectName, c))).ToArray();

                        if (tags[0].Equals("<< Reset Filter"))
                        {
                            var scanResults = client.Search<CAExcelData>(s => s
                                               .Query(q => q
                                               .FunctionScore(fs => fs
                                               .Query(fq => fq.MatchAll())
                                               .Functions(f => f.RandomScore(Guid.NewGuid().ToString()))))
                                               .Aggregations(a => a
                                                            .Terms("by_projects", t => t
                                                                .Field(f => f.ProjectName)
                                                                .Size(10000)
                                                                ))
                                                .Size(10000));


                            result = new SearchResult<CAExcelData>
                            {
                                Total = (int)scanResults.Total,
                                Page = 1,
                                Results = scanResults.Documents,
                                ElapsedMilliseconds = scanResults.Took,
                                AggregationsByTags = scanResults.Aggs.Terms("by_projects").Buckets.ToDictionary(x => x.Key, y => y.DocCount.GetValueOrDefault(0)),
                                Resultdatatable = scanResults.Documents.ToDataTable()
                            };
                        }
                        else
                        {
                            var searchresult = client.Search<CAExcelData>(x => x
                                                .Query(q => q
                                                    .Bool(b => b
                                                        .Must(m => m
                                                            .MultiMatch(mp => mp
                                                                .Query(query)
                                                                    .Fields(f => f
                                                                        .Fields(f1 => f1.ProjectName))))
                                                        .Filter(f => f
                                                            .Bool(b1 => b1
                                                                .Must(filters)))))
                                                .Aggregations(a => a
                                                    .Terms("by_projects", t => t
                                                        .Field(f => f.ProjectName)
                                                         .Size(10000))
                                                    )
                                                    .Size(10000)
                                                );

                            result = new SearchResult<CAExcelData>
                            {
                                Total = (int)searchresult.Total,
                                Page = 1,
                                Results = searchresult.Documents,
                                ElapsedMilliseconds = searchresult.Took,
                                AggregationsByTags = searchresult.Aggs.Terms("by_projects").Buckets.ToDictionary(x => x.Key, y => y.DocCount.GetValueOrDefault(0)),
                                Resultdatatable = searchresult.Documents.ToDataTable()
                            };
                        }


                    }

                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        public SearchResult<DOIExcelData> DOISearchIndex(string indexname, List<string> tags, bool isfilter, string query)
        {
            SearchResult<DOIExcelData> result = null;
            try
            {
                if (indexname == ElasticConfig.DOIresourceindexName)
                {
                    var scanResults = client.Search<DOIExcelData>(s => s
                                              .Query(q => q
                                              .FunctionScore(fs => fs
                                              .Query(fq => fq.MatchAll())
                                              .Functions(f => f.RandomScore(Guid.NewGuid().ToString()))))
                                              .Size(10000));


                    result = new SearchResult<DOIExcelData>
                    {
                        Total = (int)scanResults.Total,
                        Page = 1,
                        Results = scanResults.Documents,
                        ElapsedMilliseconds = scanResults.Took,
                        //AggregationsByTags = scanResults.Aggs.Terms("by_projects").Buckets.ToDictionary(x => x.Key, y => y.DocCount.GetValueOrDefault(0)),
                        Resultdatatable = scanResults.Documents.ToDataTable()
                    };

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        public IEnumerable<string> Suggest(string query)
        {

            var resulttest = client.Search<CAExcelData>(s => s
.Suggest(ss => ss
    .Term("post-a-suggestions", t => t
        //.MaxEdits(1)
        //.MaxInspections(2)
        //.MaxTermFrequency(3)
        //.MinDocFrequency(4)
        //.MinWordLength(5)
        //.PrefixLength(6)
        //.SuggestMode(SuggestMode.Always)
        //.Analyzer("standard")
        .Field(p => p.AssociateName)
        //.ShardSize(7)
        //.Size(8)
        .Text(query)
    )
    .Completion("post-b-suggestions", c => c
        .Fuzzy(f => f
        //.Fuzziness(Fuzziness.Auto)
        //.MinLength(1)
        //.PrefixLength(2)
        //.Transpositions()
        //.UnicodeAware(false)
        )
        .Analyzer("standard")
        .Field(p => p.AssociateName)
        .Size(8)
    )
));
            var result = client.Search<CAExcelData>(x => x.Suggest(r => r.Term("post-suggestions", t => t.Text(query))
                .Completion("post-suggestions", t => t.Analyzer("standard")
                .Field(f => f.AssociateName)
                .Size(6))));
            //.Completion("ca-comp", c => c.Field(f => f.AssociateName))));

            return result.Suggest["post-suggestions"].SelectMany(x => x.Options).Select(y => y.Text);
        }
    }

    public static class ElasticConfig
    {
        public static string AllresourceindexName
        {
            get { return ConfigurationManager.AppSettings["AllresourceindexName"]; }
        }
        public static string DOIresourceindexName
        {
            get { return ConfigurationManager.AppSettings["DOIresourceindexName"]; }
        }


        public static string ElastisearchUrl
        {
            get { return ConfigurationManager.AppSettings["elastisearchUrl"]; }
        }

        public static IElasticClient GetClient(string indexname)
        {
            var node = new Uri(ElasticConfig.ElastisearchUrl);
            IElasticClient client = null;
            if (indexname == ElasticConfig.AllresourceindexName)
            {
                var AllconnectionSettings = new ConnectionSettings(node)
                                         .PrettyJson().DisableDirectStreaming()
                                         .OnRequestCompleted(details => Debug.WriteLine(Encoding.UTF8.GetString(details.RequestBodyInBytes)))
                                         .InferMappingFor<CAExcelData>(m => m
                                           .IndexName(indexname)
                                         );
                AllconnectionSettings.DefaultIndex(indexname);
                client = new ElasticClient(AllconnectionSettings);
            }
            if (indexname == ElasticConfig.DOIresourceindexName)
            {
                var DoiconnectionSettings = new ConnectionSettings(node)
                                         .PrettyJson().DisableDirectStreaming()
                                         .OnRequestCompleted(details => Debug.WriteLine(Encoding.UTF8.GetString(details.RequestBodyInBytes)))
                                         .InferMappingFor<DOIExcelData>(m => m
                                           .IndexName(indexname)
                                         );
                DoiconnectionSettings.DefaultIndex(indexname);
                client = new ElasticClient(DoiconnectionSettings);
            }
            return client;
        }


    }

    public static class Extensions
    {
        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(
                  this IEnumerable<TSource> source, int size)
        {
            TSource[] bucket = null;
            var count = 0;

            foreach (var item in source)
            {
                if (bucket == null)
                    bucket = new TSource[size];

                bucket[count++] = item;
                if (count != size)
                    continue;

                yield return bucket;

                bucket = null;
                count = 0;
            }

            if (bucket != null && count > 0)
                yield return bucket.Take(count);
        }
    }

    public static class HtmlRemoval
    {
        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", String.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, Empty);
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }

    public static class ExcelTableProcess
    {
        public static IEnumerable<T> ConvertTableToObjects<T>(this ExcelTable table) where T : new()
        {
            //DateTime Conversion
            var convertDateTime = new Func<double, DateTime>(excelDate =>
            {
                if (excelDate < 1)
                    throw new ArgumentException("Excel dates cannot be smaller than 0.");

                var dateOfReference = new DateTime(1900, 1, 1);

                if (excelDate > 60d)
                    excelDate = excelDate - 2;
                else
                    excelDate = excelDate - 1;
                return dateOfReference.AddDays(excelDate);
            });

            //Get the properties of T
            var tprops = (new T())
                .GetType()
                .GetProperties()
                .ToList();

            //Get the cells based on the table address
            var groups = table.WorkSheet.Cells[table.Address.Start.Row, table.Address.Start.Column, table.Address.End.Row, table.Address.End.Column]
                .GroupBy(cell => cell.Start.Row)
                .ToList();

            //Assume the second row represents column data types (big assumption!)
            var types = groups
                .Skip(1)
                .First()
                .Select(rcell => rcell.Value.GetType())
                .ToList();

            //Assume first row has the column names
            var colnames = groups
                .First()
                .Select((hcell, idx) => new { Name = hcell.Value.ToString(), index = idx })
                .Where(o => tprops.Select(p => p.Name).Contains(o.Name))
                .ToList();

            //Everything after the header is data
            var rowvalues = groups
                .Skip(1) //Exclude header
                .Select(cg => cg.Select(c => c.Value).ToList());


            //Create the collection container
            var collection = rowvalues
                .Select(row =>
                {
                    var tnew = new T();
                    colnames.ForEach(colname =>
                    {
                        //This is the real wrinkle to using reflection - Excel stores all numbers as double including int
                        var val = row[colname.index];
                        var type = types[colname.index];
                        var prop = tprops.First(p => p.Name == colname.Name);

                        //If it is numeric it is a double since that is how excel stores all numbers
                        if (type == typeof(double))
                        {
                            //Unbox it
                            var unboxedVal = (double)val;

                            //FAR FROM A COMPLETE LIST!!!
                            if (prop.PropertyType == typeof(Int32))
                                prop.SetValue(tnew, (int)unboxedVal);
                            else if (prop.PropertyType == typeof(double))
                                prop.SetValue(tnew, unboxedVal);
                            else if (prop.PropertyType == typeof(DateTime))
                                prop.SetValue(tnew, convertDateTime(unboxedVal));
                            else
                                throw new NotImplementedException(String.Format("Type '{0}' not implemented yet!", prop.PropertyType.Name));
                        }
                        else
                        {
                            //Its a string
                            prop.SetValue(tnew, val);
                        }
                    });

                    return tnew;
                });


            //Send it back
            return collection;
        }

        public static DataTable getDataTableFromExcel(string path)
        {
            using (var pck = new ExcelPackage())
            {
                using (var stream = File.OpenRead(path))
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                ws.Column(ws.Dimension.End.Column).Style.Numberformat.Format = "mm/dd/yyyy";
                DataTable tbl = new DataTable();
                bool hasHeader = true; // adjust it accordingly( i've mentioned that this is a simple approach)
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                if (tbl.Columns[tbl.Columns.Count - 1].ColumnName.ToUpperInvariant() == "DATE")
                    tbl.Columns[tbl.Columns.Count - 1].DataType = typeof(DateTime);
                var startRow = hasHeader ? 2 : 1;
                for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    var row = tbl.NewRow();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                    tbl.Rows.Add(row);
                }
                return tbl;
            }
        }

        public static DataTable ToDataTable<TSource>(this IEnumerable<TSource> data)
        {
            DataTable dataTable = new DataTable(typeof(TSource).Name);
            PropertyInfo[] props = typeof(TSource).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ??
                    prop.PropertyType);
            }

            foreach (TSource item in data)
            {
                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
    }
}
