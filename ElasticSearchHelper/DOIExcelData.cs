﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchHelper
{
    public class DOIExcelData
    {
        public string ID { get; set; }
        public string CognizantID { get; set; }
        public string Name { get; set; }
        public string Startdate { get; set; }
        public string EndDate { get; set; }
        public string Linemanager { get; set; }
        public string Location { get; set; }
        public string Active { get; set; }
        public string DeclarationofInterest { get; set; }
        public string Remarks { get; set; }
    }
}
