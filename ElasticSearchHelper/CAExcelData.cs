﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchHelper
{
    public class CAExcelData
    {
        public string ID { get; set; }
        public string AssociateID { get; set; }
        public string AssociateName { get; set; }
        public string GradeName { get; set; }
        public string MonthOfJoining { get; set; }
        public string YearOfJoining { get; set; }
        public string CategoryName { get; set; }
        public string SubcategoryName { get; set; }
        public string UtilizationLocation { get; set; }
        public string LocationGroupName { get; set; }
        public string IsOnsite { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectType { get; set; }
        public string ProjectBillability { get; set; }
        public string CustomerID { get; set; }
        public string Customername { get; set; }
        public string DepartmentID { get; set; }
        public string Department { get; set; }
        public string VerticalName { get; set; }
        public string HorizontalName { get; set; }
        public string SubVertical { get; set; }
        public string SubHorizontal { get; set; }
        public string ManagerID { get; set; }
        public string ManagerName { get; set; }
        public string WeightedAverage { get; set; }
        public string LocationAvailableHours { get; set; }
        public string AvailableHours { get; set; }
        public string BilledHours { get; set; }
        public string UtilizationPercentage { get; set; }
        public string Status { get; set; }
        public string BilledFTE { get; set; }
        public string TotalFTE { get; set; }
        public string UnbilledFTE { get; set; }
        public string BU { get; set; }
        public string Business_Unit_Desc { get; set; }
        public string SBU { get; set; }
        public string ParentCustomer { get; set; }
        public string GlobalMarket { get; set; }
        public string GroupDesc { get; set; }
        public string AllocationPrecentage { get; set; }
        public string ResourceBillability { get; set; }
        public string AllocationStartDate { get; set; }
        public string AllocationEndDate { get; set; }
        public string ExpInMonths { get; set; }
        public string CriticalFlag { get; set; }
        public string LocationCode { get; set; }
        public string LocationDescription { get; set; }
        public string JobCode { get; set; }
        public string Designation { get; set; }
        public string OperationalRole { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string ActualPrediction { get; set; }
        public string Geography { get; set; }
        public string PaycostDepartmentID { get; set; }
        public string ProjectStartdate { get; set; }
        public string ProjectEnddate { get; set; }
        public string CCARoleCode { get; set; }
        public string CCARoleDescription { get; set; }
        public string ServiceLineCode { get; set; }
        public string ServiceLineDesc { get; set; }
        public string TrackCode { get; set; }
        public string TrackDesc { get; set; }
        public string SubTrackCode { get; set; }
        public string SubTrackDesc { get; set; }
        public string sharedpoolresource { get; set; }
        public string assignstatetag { get; set; }
        public string billabilityreason { get; set; }
        public string assignstatetagdesc { get; set; }
    }
}
